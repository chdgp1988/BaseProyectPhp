<!DOCTYPE html>
<html lang="es">
<head>
<?php include "{$_RUTA_THEME}/head.php"; ?>
</head>
<body>
    <?php include "{$_RUTA_THEME}/nav.php"; ?>
    <div id="layoutSidenav">
        <?php include "{$_RUTA_THEME}/menu.php"; ?>

        <div id="layoutSidenav_content">
                <main class="">
                    <div class="container-fluid px-4">
                        <h1 class="mt-4 fs-3">Blank</h1>

                    </div>
                </main>
            <?php include "{$_RUTA_THEME}/footer.php"; ?>
        </div>
        
    </div>
    <?php include "{$_RUTA_THEME}/script.php"; ?>
</body>
</html>
