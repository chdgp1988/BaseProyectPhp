<!DOCTYPE html>
<html lang="es">
<head>
<?php include 'head.php'; ?>
</head>
<body>
    <?php include 'nav.php'; ?>
    <div id="layoutSidenav">
        <?php include 'menu.php'; ?>

        <div id="layoutSidenav_content">
                <main class="">
                    <div class="container-fluid px-4">
                        <h1 class="mt-4 fs-3">Static Navigation</h1>

                    </div>
                </main>
            <?php include 'footer.php'; ?>
        </div>
        
    </div>
    <?php include 'script.php'; ?>
</body>
</html>
